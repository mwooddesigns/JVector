"use strict";

function JVector() {
    this.x = arguments[0];
    this.y = arguments[1];
    if (arguments.length >= 3) {
        this.z = arguments[2];
    }
    this.size = arguments.length;
}

JVector.prototype.print = function() {
    var output;
    switch (this.size) {
      case 2:
        output = "{x: " + this.x + ", y: " + this.y + "}";
        break;

      case 3:
        output = "{x: " + this.x + ", y: " + this.y + ", z: " + this.z + "}";
        break;

      default:
        break;
    }
    console.log(output);
};

JVector.prototype.random2D = function() {
    this.x = (Math.random() - .5) * 2;
    this.y = (Math.random() - .5) * 2;
    this.z = null;
    this.size = 2;
    this.normalize();
};

JVector.prototype.random3D = function() {
    this.x = (Math.random() - .5) * 2;
    this.y = (Math.random() - .5) * 2;
    this.z = (Math.random() - .5) * 2;
    this.size = 3;
    this.normalize();
};

JVector.prototype.set = function() {
    this.x = arguments[0];
    this.y = arguments[1];
    if (arguments.length == 2) {
        this.size = 2;
        this.z = 0;
    }
    if (arguments.length >= 3) {
        this.size = 3;
        this.z = arguments[2];
    }
};

JVector.prototype.copy = function() {
    return this;
};

JVector.prototype.add = function(vector) {
    this.x += vector.x;
    this.y += vector.y;
    if (this.size >= 3 && vector.size >= 3) {
        this.z += vector.z;
    }
};

JVector.prototype.sub = function(vector) {
    this.x -= vector.x;
    this.y -= vector.y;
    if (this.size >= 3 && vector.size >= 3) {
        this.z -= vector.z;
    }
};

JVector.prototype.mult = function(scalar) {
    this.x *= scalar;
    this.y *= scalar;
    if (this.size >= 3) {
        this.z *= scalar;
    }
};

JVector.prototype.div = function(scalar) {
    this.x /= scalar;
    this.y /= scalar;
    if (this.size >= 3) {
        this.z /= scalar;
    }
};

JVector.prototype.dist = function(vector) {
    if (this.size != vector.size) {
        console.log("Error: Vector Size Mismatch");
        return;
    } else {
        var distTotal = Math.pow(this.x - vector.x, 2) + Math.pow(this.y - vector.y, 2);
        console.log(distTotal);
    }
};

JVector.prototype.mag = function() {
    var output;
    switch (this.size) {
      case 2:
        output = Math.sqrt(this.x * this.x + this.y * this.y);
        break;

      case 3:
        output = Math.sqrt(this.x * this.x + this.y * this.y + this.z * this.z);
        break;

      default:
        break;
    }
    return output;
};

JVector.prototype.magSq = function() {
    return this.mag() * this.mag();
};

JVector.prototype.normalize = function() {
    var m = this.mag();
    this.div(m);
};