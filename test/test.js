var test2D = new JVector(10, 15);
var test3D = new JVector(20, 15, 10);

describe("Set method", function() {
  it("Sets the values of a 2D vector", function() {
    test2D.set(2, 4);
    expect(test2D.x).toBe(2);
    expect(test2D.y).toBe(4);
  });
  it("Sets the values of a 3D vector", function() {
    test3D.set(6, 8, 10);
    expect(test3D.x).toBe(6);
    expect(test3D.y).toBe(8);
    expect(test3D.z).toBe(10);
    expect(test3D.size).toBe(3);
  });
  it("Sets a 2D vector to a 3D vector", function() {
    test2D.set(5, 7, 9);
    expect(test2D.x).toBe(5);
    expect(test2D.y).toBe(7);
    expect(test2D.z).toBe(9);
    expect(test2D.size).toBe(3);
    test2D.set(10, 15);
  });
  it("Sets a 3D vector to a 2D vector", function() {
    test3D.set(9, 23);
    expect(test3D.x).toBe(9);
    expect(test3D.y).toBe(23);
    expect(test3D.z).toBe(0);
    expect(test3D.size).toBe(2)
    test3D.set(20, 15, 10);
  });
});

describe("Add method", function() {
  it("Adds a 2D input vector to a 2D vector", function() {
    test2D.add(new JVector(15, 15));
    expect(test2D.x).toBe(25);
    expect(test2D.y).toBe(30);
    test2D.set(10, 15);
  });
  it("Adds a 3D input vector to a 3D vector", function() {
    test3D.add(new JVector(15, 15, 10));
    expect(test3D.x).toBe(35);
    expect(test3D.y).toBe(30);
    expect(test3D.z).toBe(20);
    test3D.set(20, 15, 10);
  });
});

describe("Subtract method", function() {
  it("Subtracts a 2D input vector from a 2D vector", function() {
    test2D.sub(new JVector(10, 15));
    expect(test2D.x).toBe(0);
    expect(test2D.y).toBe(0);
    test2D.set(10,15);
  });
});
